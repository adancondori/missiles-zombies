/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interview.controller;

import interview.model.Board;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * Board
 *
 * Boad sera el controlador o manejador del juego
 *
 * Fecha 15-10-2015
 *
 * @author adancondori
 */
public class Launching {

    public Board board;
    public String fileName = "";
    public BufferedReader in = null;

    /**
     * Ruta del archivo de donde se quiere leer
     *
     * @param pathFile
     */
    public Launching(String pathFile) {
        this.fileName = pathFile;
        board = new Board();
        try {
            in = new BufferedReader(new FileReader(this.fileName));
        } catch (FileNotFoundException ex) {
            System.err.println("Error!!! El Archivo no se puede encontrar:\nIngrese nuevamente la ruta.");
            //Logger.getLogger(Launching.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     *Controla la lógica de juego
     * @return String //muestra los disparos disponibles
     */
    public String missileLaunching() {
        String missiles = "";//-----variable para acumular 
        if (in == null) {
            return missiles;
        }
        try {

            String line = in.readLine();
            System.out.println("Input: ");
            while (line != null && board.getCont() <= Board.NRO_SHOT)// solo muestra tres ataques del archivo
            {
                String[] splited = new String[Board.NRO_ITEM];
                if (!line.trim().equals("")) {
                    line = line.replaceAll("^ +| +$|( )+", "$1");//delete more space
                    splited = line.split("\\s+");//split space
                    if (splited.length < 2 || splited.length > 3) {//valida la cantidad de ITEM
                    } else if (board.getCont() == 0) {//first line
                        board.setFila(Integer.valueOf(splited[0]));
                        board.setCol(Integer.valueOf(splited[1]));
                        board.setCont(board.getCont() + 1);
                        if (board.getFila() == board.getCol()) {//verifica si es rectangulo
                            System.out.println("Solo se admite rectangulo, Fil: " + board.getFila() + " - Col: " + board.getCol() + "  No se permite Celda Cuadrada. ");
                            return "\nIngrese nuevamente nro de Fila y Columna";
                        }
                    } else {// lee datos, missiles 
                        if (Integer.valueOf(splited[2]) > 0) {
                            board.setCont(board.getCont() + 1);
                            missiles = missiles + "\n" + splited[0] + " | " + splited[1] + " | " + splited[2];
                        }
                    }
                }
                System.out.println(line);
                line = in.readLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(Launching.class.getName()).log(Level.SEVERE, null, ex);
        }
        //System.out.println("\nOuput\n" + "Fil: " + T.getFila() + " - " + "Col: " + T.getCol());
        missiles = "\nOuput:\n" + "Fil: " + board.getFila() + " - " + "Col: " + board.getCol() +"\n============ \nFil | Col | Nro Zombi" + missiles;
        
        return missiles;
    }
}
