/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interview.model;
/*
 * Board
 *
 * Boad sera el tablero del juego
 *
 * Fecha 10-10-2015
 *
 * @author adancondori
 */

public class Board {
    //---VARIABLES DEL EJEMPLO
    public int fila = 0;//columna
    public int columna = 0;//fila 
    public int cont = 0;//variablea para leer el nro de parametros validos
    public static final  int NRO_ITEM = 3;//Nro de  item 
    public static final  int NRO_SHOT = 3;//Nro de Disparos
    
    public String missiles = "";//-----variable para acumular 

    public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public int getCol() {
        return columna;
    }

    public void setCol(int col) {
        this.columna = col;
    }

    public int getCont() {
        return cont;
    }

    public void setCont(int cont) {
        this.cont = cont;
    }

    public String getMissiles() {
        return missiles;
    }

    public void setMissiles(String missiles) {
        this.missiles = missiles;
    }

    @Override
    public String toString() {
        return super.toString(); //To change body of generated methods, choose Tools | Templates.
    }    
}
